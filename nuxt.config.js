
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title:'高思教育',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0' },
      { name: 'keywords', name: 'keywords',content: '小升初,中考,高考,课外辅导,教育培训,补习班,家长社区,中小学辅导班,高思官网,高思教育' },
      { hid: 'description', name: 'description', content: '高思教育立志做高品质教育，专注于中小学的课外辅导培训，提供中小学最新资讯及学习方法，家教式一对一服务，高思教育让孩子爱上学习，收获成长！' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet',  href: 'https://res-static.gaosiedu.com/www/static/css/app.c33af1fedbe58679c1ac25541a8738ff.css' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~assets/style/border.css',
    '~assets/style/reset.css',
    '~assets/style/mobile.css',
    '~assets/style/toast.css',
    '~assets/style/iconfont.css',
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    '~/plugins/request.js',
    '~/plugins/toast.js',
    '~/plugins/vue-inject.js',
    { src: '~plugins/swiper', ssr: false },
    '~/plugins/ctx-inject.js'
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
  ],
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    extend(config, ctx) {
      const sassResourcesLoader = {
        loader: 'sass-resources-loader',
        options: {
          resources: ['assets/style/mixins.less', 'assets/style/varibles.less']
        }
      }
      config.module.rules.forEach(rule => {
        if (rule.test.toString() === '/\\.vue$/') {
          rule.options.loaders.less.push(sassResourcesLoader)
        }
        if (rule.test.toString() === '/\\.less$/') {
          rule.use.push(sassResourcesLoader)
        }
      })
    },
    loader:[
      {
        test:/\.less$/,
        loader:'style-loader!css-loader!less-loader',
      }
    ]
  }
}
