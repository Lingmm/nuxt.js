import axios from 'axios'
import Vue from 'vue'
import qs from 'qs'
import 'babel-polyfill' // ie8-11
import store from '@/store'

// axios.defaults.baseURL = 'https://www.gaosiedu.com/restapi';
// axios.defaults.baseURL = 'https://dev.gaosiedu.com/restapi';
axios.defaults.baseURL = 'https://test.gaosiedu.com/restapi';
//axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

export default function request(option, success,fail){
  // 登录支付不显示
  // if(router.history.current.name != 'payWeixin'){
  //   Vue.prototype.$loading('数据加载中')
  // }
  let url;
  if(option.query){
    option.query = sortQuery(option.query);
  }

  if(option.query == ''){
    url =  option.api;
  }else {
    url =  option.api + '?' + option.query;
  }

  if(option.type == 'post'){
    option.data = option.data;
  }

  option.query = option.query;

  url = option.defaultUrl ?  option.defaultUrl + url : url  //defaultUrl为Mock数据接口
  console.log(option.Accept)
  axios({  
    url: url,
    method: option.type,
    data: option.data || {},
    headers: {
      'GS-Token': '',
      'Accept': option.Accept ? option.Accept : 'application/vnd.gsapi.v1+json',
      'Access-Control-Allow-Origin': '*'
    }
  }).then(function(res){
    // 微信支付不显示
    // if(!process.server&&router.history.current.name != 'payWeixin'){
    //   Vue.prototype.$loading.close()
    // }
    var res = res.data;
    if(res.error_code == 0){
      success(res)
      console.log(res)
    } else if(res.error_code == 1001 || res.error_code == 1009 ){
      if(!process.server) {
        Vue.prototype.$toast.center('未登录，请先登录');
        //router.push('/loginCode')
        localStorage.removeItem('user');
        router.replace({
          path: "/loginCode",
          query: {
            redirect: router.currentRoute.fullPath
          }
        })
      }
    }else if(res.error_code == 1005){
      if(!process.server)Vue.prototype.$toast.center('网络故障')
    }else if(res.error_code == 1007){
      if(!process.server)Vue.prototype.$toast.center('微信用户openid或者学员信息获取失败')
    }else if(res.error_code == 1008){
      if(!process.server)Vue.prototype.$toast.center('登录过期')
      router.push('/loginCode')
    }else if(res.error_code == 1010){
      if(!process.server)Vue.prototype.$toast.center('图形验证码输入错误')
    }else if(res.error_code == 429){
      if(!process.server)Vue.prototype.$toast.center('请求过于频繁，请稍候再试')
    }else if(res.error_code == 10002){
      if(!process.server)Vue.prototype.$toast.center('用户信息未完善')
      router.push('/p-Data')
    }else {
      if(fail!=null){
        fail(res)
      }else{
        if(!process.server) Vue.prototype.$toast.center(res.message)
      }
    }
  }).catch((err)=>{
    // if(!process.server)Vue.prototype.$loading.close()
    // if(err.response.status == 429){
    //   Vue.prototype.$toast.center('请求过于频繁，请稍候再试')
    // }else {
    //   alert(err)
    // }
  })
};

function sortQuery(obj){
  // 先获取所有属性名
  var keys = [];
  for (var key in obj){
    keys.push(key);
  }
  // 排序
  keys.sort();
  // 导出新的对象
  var r = {};
  for (var i = 0; i < keys.length; i++){
    key = keys[i];
    r[key] = obj[key];
  }
  // 将排序好的对象转为请求参数
  var str = '';
  for(var k in r){
    str += k+'='+r[k]+'&'
  }
  return str.substr(0,str.length-1);
};
