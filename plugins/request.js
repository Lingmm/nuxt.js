import Vue from 'vue'
import request from '~/plugins/http/request.js'

export default ({ app }, inject) => {
    inject('request', request)
  }