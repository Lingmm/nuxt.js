let userlist = 'user'
let numStr = 'num'
export default {
    changeUser(state,user) {
        state.user = user;
        try {
            localStorage.setItem(userlist, JSON.stringify(user))
        } catch (error) {}
    },

    changeCartNum(state,num) {
        state.num = num;
        try {
            localStorage.setItem(numStr, JSON.stringify(num))
        } catch (error) {}
    }
}