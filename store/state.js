let userlist = 'user'
let numStr = 'num'
let defaultUser = {};
let defaultNumber = 0;
try {
    if(localStorage.getItem(userlist) || localStorage.getItem(numStr)){
        defaultUser = JSON.parse(localStorage.getItem(userlist))
        defaultNumber = localStorage.getItem(numStr)
    }
}catch(e){
    if(!process.server){
        alert(e)
       }
}

export default {
    user: defaultUser,
    num: defaultNumber
}